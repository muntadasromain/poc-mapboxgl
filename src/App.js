import Map, { Source, Layer, NavigationControl } from 'react-map-gl';
import { useRef, useState } from 'react';
import bbox from '@turf/bbox'
import pako from 'pako'

import './App.css';
import 'mapbox-gl/dist/mapbox-gl.css'

import bati from './assets/31484-batiments.json'
import parcellesStGenies from './assets/31484-parcelles.json'

import communes from './assets/communes.json'
import regions from './assets/regions.json'
import departements from './assets/departements.json'

const initialViewState = {
	longitude: 1.4434618432873223,
	latitude: 43.61929716198888,
	zoom: 5
}

const borderLine = {
	id: 'borderLine',
	type: 'line',
	paint: {
		'line-color': '#003',
		'line-width': 2
	}
}

const borderFill = {
	id: 'borderFill',
	type: 'fill',
	paint: {
		'fill-color': '#003',
		'fill-opacity': [
			'case',
			['boolean', ['feature-state', 'hover'], false],
			0.3,
			0
		]
	}
}

const parcellesLine = {
	id: 'parcellesLine',
	type: 'line',
	paint: {
		'line-color': '#FFF',
		'line-width': 2
	}
};

const parcellesFill = {
	id: 'parcellesFill',
	type: 'fill',
	paint: {
		'fill-color': '#FFF',
		'fill-opacity': 0.1
	}
};

const batiLine = {
	id: 'batiLine',
	type: 'line',
	paint: {
		'line-color': '#000',
		'line-width': 1,
		'line-opacity': 0.6
	}
};

async function getCadastre(code) {
	let codeInsee = code + ""
	let dpt = codeInsee.substring(0, 2)
	if (dpt === '97') dpt = codeInsee.substring(0, 3)
	console.log("Departement: " + dpt)
	console.log("Code: " + code)
	console.log("Code Insee: " + codeInsee)
	const url = 'https://cadastre.data.gouv.fr/data/etalab-cadastre/latest/geojson/communes/' + dpt + '/' + code + '/cadastre-' + code + '-parcelles.json.gz'
	console.log("URL: " + url)
	const response = await fetch(url);
	const compressed = await response.arrayBuffer();
	const unzipped = pako.inflate(new Uint8Array(compressed));
	const decoder = new TextDecoder();
	const data = decoder.decode(unzipped);
	return data;
}

function App() {

	const mapRef = useRef(null)

	const [zoomLevel, setZoomLevel] = useState(5);

	const [showCommunes, setShowCommunes] = useState(false);
	const [showRegions, setShowRegions] = useState(true);
	const [showDpt, setShowDpt] = useState(false);
	const [showCadastre, setShowCadastre] = useState(false)
	const [currentBorder, setCurrentBorder] = useState('regions');

	const [parcelles, setParcelles] = useState(null);

	let hoveredAreaId = null

	const handleZoom = (e) => {
		setZoomLevel(e.viewState.zoom)
		console.log("Zoom : " + zoomLevel)
		if (e.viewState.zoom > 13) {
			setParcelles(parcellesStGenies)
			setShowCadastre(true)
			setShowCommunes(false)
			setShowDpt(false)
			setShowRegions(false)
			setCurrentBorder('communes')
		}
		else if (e.viewState.zoom > 10) {
			setShowCadastre(false)
			setShowCommunes(true)
			setShowDpt(false)
			setShowRegions(false)
			setCurrentBorder('communes')
		} else if (e.viewState.zoom > 7) {
			setShowCadastre(false)
			setShowCommunes(false)
			setShowDpt(true)
			setShowRegions(false)
			setCurrentBorder('departements')
		} else if (e.viewState.zoom > 0) {
			setShowCadastre(false)
			setShowCommunes(false)
			setShowDpt(false)
			setShowRegions(true)
			setCurrentBorder('regions')
		}
	}

	const handleClic = (e) => {
		const feature = mapRef.current.queryRenderedFeatures(e.point)[0];
		if (feature !== undefined) {
			if (feature.source === currentBorder) {
				const [minLng, minLat, maxLng, maxLat] = bbox(feature)

				mapRef.current.fitBounds(
					[[minLng, minLat],
					[maxLng, maxLat]],
					{ padding: 64, duration: 1000 }
				);
				console.log(feature)

				if (feature.source === 'communes') {
					getCadastre(feature.id).then((geojson) => {
						setParcelles(geojson)
						setShowCommunes(false)
						setShowCadastre(true)
						console.log("Parcelle state updated")
					})
				}
			} else if (feature.source === "parcelles") {
				let surface = feature.properties.contenance
				let nom = feature.properties.prefixe + " " + feature.properties.section + " " + feature.properties.numero
				alert("Parcelle " + nom + "\nSuperficie : " + surface + "m2")
			}
		}
	}

	const onMouseMove = (e) => {
		const feature = mapRef.current.queryRenderedFeatures(e.point)[0];
		if (feature !== undefined && feature.source === currentBorder) {
			if (feature.layer.id === borderFill.id) {
				if (hoveredAreaId !== null) {
					mapRef.current.setFeatureState({
						source: currentBorder,
						id: hoveredAreaId,
					}, { hover: false })
				}
				hoveredAreaId = feature.id
				mapRef.current.setFeatureState({
					source: currentBorder,
					id: hoveredAreaId,
				}, { hover: true })
			}
		}
	}

	const onMouseLeave = (e) => {
		if (hoveredAreaId !== null) {
			mapRef.current.setFeatureState({
				source: currentBorder,
				id: hoveredAreaId,
			}, { hover: false })
		}
	}

	return (
		<Map
			initialViewState={initialViewState}
			ref={mapRef}
			onZoomEnd={handleZoom}
			onClick={handleClic}
			onMouseMove={onMouseMove}
			onMouseLeave={onMouseLeave}
			mapStyle='mapbox://styles/mapbox/satellite-streets-v12'
			style={{ width: '100vw', height: '100vh' }}
			mapboxAccessToken='pk.eyJ1Ijoicm11bnRkcyIsImEiOiJjbGRucGtxZTgwazBpM3BxZzdpOXBsdTlkIn0.Ixv9Rh6eRAgrN0hkYWi8KA'
			interactiveLayerIds={['borderFill']}>
			{showCadastre
				?
				<Source id='parcelles' type='geojson' data={parcelles} promoteId="id">
					<Layer {...parcellesFill} />
					<Layer {...parcellesLine} />
				</Source>
				: ''
			}
			<Source id="bati" type="geojson" data={bati}>
				<Layer {...batiLine} />
			</Source>

			{
				showRegions
					? <Source id="regions" type="geojson" data={regions} promoteId="code"><Layer {...borderLine} /><Layer {...borderFill} /></Source>
					: ''
			}
			{
				showDpt
					? <Source id="departements" type="geojson" data={departements} promoteId="code"><Layer {...borderLine} /><Layer {...borderFill} /></Source>
					: ''
			}
			{
				showCommunes
					? <Source id="communes" type="geojson" data={communes} promoteId="code">
						<Layer {...borderLine} />
						<Layer {...borderFill} />
					</Source>
					: ''
			}
			<NavigationControl />
		</Map >
	);
}

export default App;
